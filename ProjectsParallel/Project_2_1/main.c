#include <omp.h> 
#include <stdio.h>

#define CHUNK 100 
#define N 10

main()
{
	int i, j, sum;
	int a[N][N];

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			a[i][j] = i * j;

#pragma omp parallel shared(a) private(i,j,sum)
#pragma omp for 
	for (i = 0; i < N; i++)
	{
		sum = 0;
		for (j = 0; j < N; j++)
			sum += a[i][j];
		printf("Sum items row %d equel %d\n", i, sum);
	}
}