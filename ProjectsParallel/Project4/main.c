#include <omp.h> 
#include <stdio.h>

#define THREADS 4 
#define N 20

int main(void)
{
	double mas[N][N + 1];
	double x[N]; 
	int otv[N]; 
	int i, j, k, n;
	double tmp;

	do
	{
		printf("Enter the number of system equations: ");
		scanf_s("%d", &n);
		if (N < n)
			printf("Too many equations. Repeat Entry\n");
	} while (N < n);
	printf("Enter system:\n");
	for (i = 0; i < n; i++)
		for (j = 0; j < n + 1; j++)
			scanf_s("%lf", &mas[i][j]);
		
	printf("System:\n");
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n + 1; j++)
			printf("%7.2f ", mas[i][j]);
		printf("\n");
	}
	
	for (i = 0; i < n + 1; i++)
		otv[i] = i;

	omp_set_num_threads(THREADS);

	for (k = 0; k < n; k++)
	{
		if (fabs(mas[k][k]) < 0.0001)
		{
			printf("The system does not have a unique solution.");
			return (0);
		}
#pragma omp parallel for private (j, k, tmp) 
		tmp = mas[k][k];
		for (j = n; j >= k; j--)
			mas[k][j] /= tmp;
		for (i = k + 1; i < n; i++)
			for (j = n; j >= k; j--)
				mas[i][j] -= mas[k][j] * mas[i][k];
	}

	for (i = 0; i < n; i++)
		x[i] = mas[i][n];
	for (i = n - 2; i >= 0; i--)
#pragma omp for private (j) 
		for (j = i + 1; j < n; j++)
			x[i] -= x[j] * mas[i][j];
	
	
	printf("Answer:\n");
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
			if (i == otv[j])
			{ 
				printf("%f\n", x[j]);
				break;
			}
	return (0);
}