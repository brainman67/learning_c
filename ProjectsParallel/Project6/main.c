#include <omp.h> 
#include <stdio.h>

double f(double x)
{
	return x;
}

double SimpsonMethod(double a, double b, int n)
{
	double h = (b - a) / n;
	double s = (f(a) + f(b)) * 0.5;
#pragma omp parallel for reduction(+:s) 
	for (int i = 1; i <= n - 1; i++)
	{
		double xk = a + h * i; //xk
		double xk1 = a + h * (i - 1); //Xk-1
		s += f(xk) + 2 * f((xk1 + xk) / 2);
	}
	double x = a + h * n; //xk
	double x1 = a + h * (n - 1); //Xk-1
	s += 2 * f((x1 + x) / 2);

	return s * h / 3.0;
}

int main() {
	int a, b, n;
	printf("Enter the a: ");
	scanf_s("%d", &a);
	printf("Enter the b: ");
	scanf_s("%d", &b);
	printf("Enter the n: ");
	scanf_s("%d", &n);

	double answer = SimpsonMethod(a, b, n);

	printf("Answer:\n");
	printf("%f\n", answer);
	return 0;
}