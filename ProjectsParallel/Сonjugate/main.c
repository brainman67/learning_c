﻿#include<stdio.h> 
#define N 20
#define E 0.00001 

double A[N][N], F[N], Xk[N], Zk[N];
double Rk[N], Sz[N], alf, bet, mf;
double Spr, Spr1, Spz;

int main()
{
	int  i, j, n;
	do
	{
		printf("Enter the number of system equations: ");
		scanf_s("%d", &n);
		if (N < n)
			printf("Too many equations. Repeat Entry\n");
	} while (N < n);

	printf("Enter system:\n");
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n + 1; j++)
		{
			if (j < n)
			{
				scanf_s("%lf", &A[i][j]);
			}
			else
			{
				scanf_s("%lf", &F[i]);
			}
		}
		mf += F[i] * F[i];
	}

	printf("System:\n");
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n + 1; j++)
			if (j < n)
			{
				printf("%7.2f ", A[i][j]);
			}
			else
			{
				printf("%7.2f ", F[i]);
			}
		printf("\n");
	}

	//початкове наближення
	for (i = 0; i < n; i++)
		Xk[i] = 0.2;

	/* початкові значення r0 і z0. */
	for (i = 0; i < n; i++)
	{
		for (Sz[i] = 0, j = 0; j < n; j++)
			Sz[i] += A[i][j] * Xk[j];
		Rk[i] = F[i] - Sz[i];
		Zk[i] = Rk[i];
	}

	do
	{
		/* чисельник і знаменник для альфи к-го
		 * αk = (rk-1,rk-1)/(Azk-1,zk-1) */

		Spz = 0;
		Spr = 0;
		for (i = 0; i < n; i++)
		{
			for (Sz[i] = 0, j = 0; j < n; j++)
				Sz[i] += A[i][j] * Zk[j];
			Spz += Sz[i] * Zk[i];
			Spr += Rk[i] * Rk[i];
		}
		alf = Spr / Spz;

		/* вираховуємо вектор рішення: xk = xk-1+ αkzk-1, і rk = rk-1-αkAzk-1 */
		Spr1 = 0;
		for (i = 0; i < n; i++)
		{
			Xk[i] += alf * Zk[i];
			Rk[i] -= alf * Sz[i];
			Spr1 += Rk[i] * Rk[i];
		}
		/* Обчислюємо  βk  */
		bet = Spr1 / Spr;

		/* Обчислюємо вектор спуску: zk = rk+ βkzk-1 */
		for (i = 0; i < n; i++)
			Zk[i] = Rk[i] + bet * Zk[i];
	}
	/* Перевірка умови виходу */
	while (Spr1 / mf > E* E);

	for (i = 0; i < n; i++)
	{
		printf("X%d - %f \n", i, Xk[i]);
	}

	return(0);
}