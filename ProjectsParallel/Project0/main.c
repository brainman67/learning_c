
#include<stdio.h>

#include<omp.h>

int main() {

#ifdef _OPENMP
	printf("OpenMP is supported!\n");
	printf("Version: %d\n", _OPENMP);
#else
	printf("OpenMP is not supported!\n");
#endif

}