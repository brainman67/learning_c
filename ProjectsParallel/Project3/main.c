#include <omp.h> 
#include <stdio.h>

double integralReduction(double a, double b, double steps)
{
	int st = steps;
	int n = 3;
	double result = 0;
	double function = 0;
	double dx = (b - a) / steps;
	double startTime = 0, endTime = 0;
	omp_set_num_threads(n);
	int i;
#pragma omp parallel firstprivate(a, dx, function)
	{
		startTime = omp_get_wtime();
#pragma omp for reduction(+:result, x) 
		for (i = 0; i < st; i++) {
			double x = a + i * dx;
			function += log(x) - (3 * sin(3 * x));
			result += function * dx;
			endTime = omp_get_wtime();
			printf("Time work: %f\nNumber potok: %d\n", endTime - startTime, omp_get_thread_num());
		}     
		return result;
	}
}

main()
{
	printf("result %f\n", integralReduction(3, 4, 5));
}