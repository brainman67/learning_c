#include <omp.h> 
#include <stdio.h>

#define CHUNK 100 
#define N 10

main()
{
	int i, j, sum, total;
	int a[N][N];

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			a[i][j] = i * j;

	total = 0;
#pragma omp parallel for shared(a) private(i, j, sum) reduction(+:total) 
	{
		for (i = 0; i < N; i++)
		{
			sum = 0;
			for (j = i; j < N; j++)
				sum += a[i][j];
			printf("Sum items row %d equel %d\n", i, sum);
			total = total + sum;
		}
		printf("\nSum all matrix items equel %d\n", total);
	}
}