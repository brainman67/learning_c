#include <stdio.h>
#include <omp.h>
#define N 100 

int a[N][N], b[N][N], c[N][N];
int main() {
	int i, j, k;
	double t1, t2;

	for (i = 1; i < N + 1; i++)
		for (j = 1; j < N + 1; j++)
			a[i - 1][j - 1] = b[i - 1][j - 1] = i * j;
	t1 = omp_get_wtime();

#pragma omp parallel for shared(a, b, c) private(i, j, k)

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			c[i][j] = 0.0;
			for (k = 0; k < N; k++)
				c[i][j] += a[i][k] * b[k][j];
		}
	}

	t2 = omp_get_wtime();
	printf("Time=%lf\n", t2 - t1);

	printf("\n -A-");
	for (i = 0; i < N; i++)
	{
		printf("\n");
		for (j = 0; j < N; j++)
		{
			printf(" %d", a[i][j]);
		}
	}

	printf("\n\n -B-");
	for (i = 0; i < N; i++)
	{
		printf("\n");
		for (j = 0; j < N; j++)
		{
			printf(" %d", b[i][j]);
		}
	}

	printf("\n\n -C-");
	for (i = 0; i < N; i++)
	{
		printf("\n");
		for (j = 0; j < N; j++)
		{
			printf(" %d", c[i][j]);
		}
	}
}